<?php 

get_header();

UtilityBelt::load_sections(array(
	'homehero',
	'summaries/features-summary',
	'summaries/addons-summary',
	'summaries/gallery-summary',
	'summaries/pricing-summary',
	'contact',
));

get_footer();

?>