<footer class="footer">
	<div class="footer-wrapper">
		<div class="footer-wrapper-copyright">Copyright &copy; <?php echo Date('Y') ?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo bloginfo('name'); ?></div>
	</div>
</footer>
<?php
	
	include( locate_template( 'components/signup.php' ) );

	if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
	   ?> <script src="//localhost:35729/livereload.js"></script> <?php
	}

	wp_footer();
?>
	</body>
</html>