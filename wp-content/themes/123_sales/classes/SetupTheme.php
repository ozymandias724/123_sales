<?php 
class SetupTheme{
	public static function _init(){
		show_admin_bar( false );
		add_action( 'after_setup_theme', 'SetupTheme::after_setup_theme' );
		add_action( 'wp_enqueue_scripts', 'SetupTheme::wp_enqueue_scripts' );
		add_action( 'init', 'SetupTheme::init' );
		add_action( 'save_post', 'SetupTheme::build_site_json' );
		add_action( 'wp_ajax_nopriv_gallery_request', 'SetupTheme::handle_gallery_request' );
		add_action( 'wp_ajax_gallery_request', 'SetupTheme::handle_gallery_request' );
	}
	public static function init(){
		SetupTheme::clean_head();

		register_nav_menu( 'main-nav', 'Main Navigation' );
	
		
		$labels = array(
			'name'                => __( 'Sites', 'text-domain' ),
			'singular_name'       => __( 'Site', 'text-domain' ),
			'add_new'             => _x( 'Add New Site', 'text-domain', 'text-domain' ),
			'add_new_item'        => __( 'Add New Site', 'text-domain' ),
			'edit_item'           => __( 'Edit Site', 'text-domain' ),
			'new_item'            => __( 'New Site', 'text-domain' ),
			'view_item'           => __( 'View Site', 'text-domain' ),
			'search_items'        => __( 'Search Sites', 'text-domain' ),
			'not_found'           => __( 'No Sites found', 'text-domain' ),
			'not_found_in_trash'  => __( 'No Sites found in Trash', 'text-domain' ),
			'parent_item_colon'   => __( 'Parent Site:', 'text-domain' ),
			'menu_name'           => __( 'Gallery Sites', 'text-domain' ),
		);
	
		$args = array(
			'labels'                   => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => null,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array('title')
		);
	
		register_post_type( 'site_gallery', $args );
		
		$labels = array(
			'name'					=> _x( 'Gallery Categories', 'Taxonomy plural name', 'text-domain' ),
			'singular_name'			=> _x( 'Gallery Category', 'Taxonomy singular name', 'text-domain' ),
			'search_items'			=> __( 'Search Gallery Categories', 'text-domain' ),
			'popular_items'			=> __( 'Popular Gallery Categories', 'text-domain' ),
			'all_items'				=> __( 'All Gallery Categories', 'text-domain' ),
			'parent_item'			=> __( 'Parent Gallery Category', 'text-domain' ),
			'parent_item_colon'		=> __( 'Parent Gallery Category', 'text-domain' ),
			'edit_item'				=> __( 'Edit Gallery Category', 'text-domain' ),
			'update_item'			=> __( 'Update Gallery Category', 'text-domain' ),
			'add_new_item'			=> __( 'Add New Gallery Category', 'text-domain' ),
			'new_item_name'			=> __( 'New Gallery Category Name', 'text-domain' ),
			'add_or_remove_items'	=> __( 'Add or remove Gallery Categories', 'text-domain' ),
			'choose_from_most_used'	=> __( 'Choose from most used Categories', 'text-domain' ),
			'menu_name'				=> __( 'Gallery Category', 'text-domain' ),
		);
		
		$args = array(
			'labels'            => $labels,
			'public'            => false,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => false,
			'show_tagcloud'     => true,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);
		
		register_taxonomy( 'gallery_category', 'site_gallery', $args );


	}
	public static function after_setup_theme(){
		add_theme_support( 'html5' );
		add_theme_support( 'post-thumbnails' );	
	}
	public static function build_site_json($post_id){
		// build json of all gallery site data if we're on localhost or 123websites.com
		if( UtilityBelt::is_123() || UtilityBelt::is_localhost() ){
			file_put_contents( get_template_directory() . "/sites.json", json_encode(UtilityBelt::get_all_gallery_site_meta()) );
		}
	}
	public static function wp_enqueue_scripts(){
		SetupTheme::register_styles();
		SetupTheme::enqueue_styles();
		SetupTheme::register_javascript();
		SetupTheme::enqueue_javascript();
		SetupTheme::localize_scripts();	
	}
	public static function handle_gallery_request(){
		$content = UtilityBelt::get_all_gallery_site_meta($_POST['offset'], 9, isset($_POST['id']) ? $_POST['id'] : null);
		if( empty($content) ){
			echo 'empty';
		}
		else{
			echo json_encode($content);
		}
		wp_die();
	}
	private static function clean_head(){
		// removes generator tag
		remove_action( 'wp_head' , 'wp_generator' );
		// removes dns pre-fetch
		remove_action( 'wp_head', 'wp_resource_hints', 2 );
		// removes weblog client link
		remove_action( 'wp_head', 'rsd_link' );
		// removes windows live writer manifest link
		remove_action( 'wp_head', 'wlwmanifest_link');	
	}
	private static function enqueue_javascript(){
		wp_enqueue_script( 'select2' );
		wp_enqueue_script( 'theme' );
	}
	private static function enqueue_styles(){
		wp_enqueue_style( 'select2' );
		wp_enqueue_style( 'theme' );
	}

	private static function register_javascript(){
		wp_register_script( 'theme', get_template_directory_uri() . '/build/js/build.js' );
	}

	private static function register_styles(){
		wp_register_style( 'select2', get_template_directory_uri() . '/build/css/select2.css' );
		wp_register_style( 'theme', get_template_directory_uri() . '/build/css/build.css' );
	}
	private static function localize_scripts(){
		wp_localize_script( 'theme', 'WPAJAX', admin_url('admin-ajax.php') );
	}
}

SetupTheme::_init();

?>