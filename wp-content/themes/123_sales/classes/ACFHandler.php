<?php 
	class ACFHandler{
		public static function _init(){
			add_action( 'acf/init', 'ACFHandler::register_fields' );
			add_action( 'acf/init', 'ACFHandler::add_options_pages' );
			add_action( 'acf/input/admin_footer', 'ACFHandler::change_slideshow_button_label' );
			add_action( 'acf/update_value', 'ACFHandler::action_acf_update_value', 10, 3 );
		}
		public static function register_fields(){
			acf_add_local_field_group(array(
				'key' => 'group_98vzczvidsf123',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_adf0dijafah123sda',
						'label' => 'Site Title',
						'type' => 'text',
						'name' => 'site-title',
					),
					array(
						'key' => 'field_adfs9jisdaf12a',
						'label' => 'Site Description',
						'type' => 'text',
						'name' => 'site-description',
					),
					array(
						'key' => 'field_89czvuqdsaf',
						'label' => 'Logo',
						'type' => 'image',
						'name' => 'logo',
						'return_format' => 'url',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'site-settings',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_f8a0dsf8yh1',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_adf8ouadf123',
						'label' => 'Hero',
						'type' => 'tab',
					),
					array(
						'key' => 'field_czv9x8hudfafgu',
						'label' => 'Slideshow or Video?',
						'type' => 'select',
						'name' => 'homehero_type_select',
						'choices' => array(
							'slideshow' => 'Slideshow',
							'video' => 'Video',
						),
						'wrapper' => array(
							'width' => 40,
						),
					),
					array(
						'key' => 'field_9217hdsafh',
						'label' => 'Hero Video',
						'type' => 'file',
						'return_format' => 'url',
						'name' => 'homehero_video',
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_czv9x8hudfafgu',
									'operator' => '==',
									'value' => 'video',
								)
							),
						),
					),
					array(
						'key' => 'field_ocxvhizcn',
						'label' => 'Slideshow',
						'type' => 'gallery',
						'name' => 'homehero_slideshow',
						'min' => 3,
						'instructions' => 'Minimum of 3 images required',
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_czv9x8hudfafgu',
									'operator' => '==',
									'value' => 'slideshow',
								)
							),
						),
					),
					array(
						'key' => 'field_dfa8dhan',
						'label' => 'Features',
						'type' => 'tab',
					),
					array(
						'key' => 'field_9czhvh123',
						'label' => 'Superheader',
						'instructions' => 'This is the text that appears above the header',
						'name' => 'features_superheader',
						'type' => 'text',
					),
					array(
						'key' => 'field_0czvuhzkja123',
						'label' => 'Header',
						'name' => 'features_header',
						'type' => 'text',
					),
					array(
						'key' => 'field_98vhuzcxvjiu',
						'label' => '<h2 style="font-weight: bold; color: red;">To select the features that appear on the home page <span style="color: grey;">(you will want to do this or else the home page will look weird).</span> Go to Site Settings > Features Page.</h2>',
						'type' => 'message',
					),
					array(
						'key' => 'field_98czvhadsf',
						'label' => 'Add-ons',
						'type' => 'tab',
					),
					array(
						'key' => 'field_9v8zh13adf',
						'label' => 'Superheader',
						'instructions' => 'This is the text that appears above the header',
						'name' => 'addons_superheader',
						'type' => 'text',
					),
					array(
						'key' => 'field_0czafaa123',
						'label' => 'Header',
						'name' => 'addons_header',
						'type' => 'text',
					),
					array(
						'key' => 'field_izcadafsf',
						'label' => 'List of Add-ons',
						'type' => 'repeater',
						'name' => 'addons',
						'button_label' => 'Add New Item',
						'sub_fields' => array(
							array(
								'key' => 'field_08vxzihczjxn',
								'label' => 'Icon',
								'type' => 'image',
								'return_format' => 'url',
								'name' => 'image',
							),
							array(
								'key' => 'field_czuvhuh',
								'name' => 'title',
								'label' => 'Title',
								'type' => 'text',
							),
							array(
								'key' => 'field_c32uqfoaadaf',
								'name' => 'description',
								'label' => 'Description',
								'type' => 'textarea',
								'new_lines' => 'br',
							),
							array(
								'key' => 'field_c98ovzhvovz',
								'label' => 'Price',
								'name' => 'price',
								'type' => 'number',
								'prepend' => '$',
							),
						),
					),
					array(
						'key' => 'field_8ocvizhlvl2',
						'label' => 'Pricing',
						'type' => 'tab',
					),
					array(
						'key' => 'field_9afdoh123qfaf',
						'label' => 'Superheader',
						'instructions' => 'This is the text that appears above the header',
						'name' => 'pricing_superheader',
						'type' => 'text',
					),
					array(
						'key' => 'field_cvz98210ihdsaf',
						'label' => 'Header',
						'name' => 'pricing_header',
						'type' => 'text',
					),
					array(
						'key' => 'field_908cxozvhix',
						'label' => '<h1 style="color: red;">Looking for something?</h1>
								<div>
								Want to add features to the homepags\'s pricing table? - Please see Site Settings > Features Page > List of Features.<br/>
								</div>',
						'type' => 'message',
					),
					array(
						'key' => 'field_982ohifaedafdsa',
						'label' => 'Gallery',
						'type' => 'tab',
					),
					array(
						'key' => 'field_928ohfaafwa',
						'label' => 'Superheader',
						'instructions' => 'This is the text that appears above the header',
						'name' => 'gallery_superheader',
						'type' => 'text',
					),
					array(
						'key' => 'field_01392oihwefa',
						'label' => 'Header',
						'name' => 'gallery_header',
						'type' => 'text',
					),
					array(
						'key' => 'field_98cyzvoh123asdf',
						'label' => '<h2 style="font-weight: bold; color: red;">To select the sites that appear on the home page <span style="color: grey;">(you will want to do this or else the home page will look weird).</span> Go to Site Gallery</h2>',
						'type' => 'message',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'home-page',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_9cv8czhodfuha',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_8cvozihcv',
						'label' => 'Hero Image',
						'type' => 'image',
						'name' => 'features-hero-image',
						'return_format' => 'url',
					),	
					array(
						'key' => 'field_8v9czoichv',
						'label' => 'Hero Tagline',
						'type' => 'text',
						'name' => 'features-hero-tagline',
					),
					array(
						'key' => 'field_9c8vzohcczvaf',
						'label' => 'Disable Hero Button?',
						'type' => 'true_false',
						'name' => 'features-disable-hero-button',
						'ui' => true,
					),
					array(
						'key' => 'field_uchzv08123h',
						'label' => 'Hero Button Link',
						'type' => 'link',
						'name' => 'features-hero-button-link',
						'instructions' => 'This controls the button link and text',
					),
					array(
						'key' => 'field_cxovuizhc123',
						'label' => 'List of Features',
						'type' => 'repeater',
						'name' => 'features',
						'button_label' => 'Add New Feature',
						'sub_fields' => array(
							array(
								'key' => 'field_o8zvcohif',
								'label' => 'Image',
								'type' => 'image',
								'name' => 'image',
								'return_format' => 'url', 
							),
							array(
								'key' => 'field_iuczvhz21',
								'label' => 'Title',
								'type' => 'text',
								'name' => 'title',
							),
							array(
								'key' => 'field_90cvzh',
								'label' => 'Description',
								'type' => 'textarea',
								'name' => 'description',
							),
							array(
								'key' => 'field_ocuzvhzkuhd',
								'label' => 'On Home Page?',
								'type' => 'true_false',
								'name' => 'on-home-page',
								'ui' => true,
							),
							array(
								'key' => 'field_q983yhoufa',
								'label' => 'On Pricing Table?',
								'type' => 'true_false',
								'name' => 'on-pricing-table',
								'ui' => true,
							),
						),
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'features-page',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_12e9ads8foiuh',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_928yafouihksd',
						'label' => 'Hero Image',
						'type' => 'image',
						'name' => 'gallery-hero-image',
						'return_format' => 'url',
					),	
					array(
						'key' => 'field_2308oiafdaadsf',
						'label' => 'Hero Tagline',
						'type' => 'text',
						'name' => 'gallery-hero-tagline',
					),
					array(
						'key' => 'field_12039ueoafidhds',
						'label' => 'Disable Hero Button?',
						'type' => 'true_false',
						'name' => 'gallery-disable-hero-button',
						'ui' => true,
					),
					array(
						'key' => 'field_01293uwaofihd',
						'label' => 'Hero Button Link',
						'type' => 'link',
						'name' => 'gallery-hero-button-link',
						'instructions' => 'This controls the button link and text',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'gallery-page',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_dasfasd9f8chouvxz',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_daf98uhadsf',
						'label' => 'Hero Image',
						'type' => 'image',
						'name' => 'contact-hero-image',
						'return_format' => 'url',
					),	
					array(
						'key' => 'field_zcv98oauhdf',
						'label' => 'Hero Tagline',
						'type' => 'text',
						'name' => 'contact-hero-tagline',
					),
					array(
						'key' => 'field_09oiahfjnadsf',
						'label' => 'Disable Hero Button?',
						'type' => 'true_false',
						'name' => 'contact-disable-hero-button',
						'ui' => true,
					),
					array(
						'key' => 'field_97syiafufdasj',
						'label' => 'Hero Button Link',
						'type' => 'link',
						'name' => 'contact-hero-button-link',
						'instructions' => 'This controls the button link and text',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'contact-page',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_0dsf8aoihdaff',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_o8zchovsa21afd',
						'label' => 'Description',
						'name' => 'description',
						'type' => 'textarea',
						'new_lines' => 'br',
						'instructions' => 'New lines here will translate to the frontend'
					),
					array(
						'key' => 'field_ocvizdasf',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'return_format' => 'url',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'feature',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'field_98covihzcuhdfa',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_d98saohizxcv',
						'label' => 'URL',
						'type' => 'url',
						'name' => 'url',
					),
					array(
						'key' => 'field_1029eoihfdssa',
						'label' => 'Image',
						'type' => 'image',
						'name' => 'image',
						'return_format' => 'url', 
					),
					array(
						'key' => 'field_10239ouwiefadhls',
						'label' => 'Description',
						'type' => 'textarea',
						'name' => 'description',
					),
					array(
						'key' => 'field_xzcv09uiojqlkadfs',
						'label' => 'On Home Page?',
						'type' => 'true_false',
						'name' => 'on-home-page',
						'ui' => true,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'site_gallery',
						),
					),
				),
			));
		}
		public static function add_options_pages(){
			$parent = acf_add_options_page(array(
				'page_title' 	=> ' ',
				'menu_title'	=> 'Site Settings',
				'menu_slug' 	=> 'site-settings',
				'capability'	=> 'edit_posts',
				'icon_url'      => 'dashicons-admin-settings',
				'redirect'		=> false,
				'position'      => 4,
			));
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Home Page',
				'menu_title' 	=> 'Home Page',
				'menu_slug'     => 'home-page',
				'parent_slug' 	=> $parent['menu_slug'],
			));
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Features Page',
				'menu_title' 	=> 'Features Page',
				'menu_slug'     => 'features-page',
				'parent_slug' 	=> $parent['menu_slug'],
			));
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Gallery Page',
				'menu_title' 	=> 'Gallery Page',
				'menu_slug'     => 'gallery-page',
				'parent_slug' 	=> $parent['menu_slug'],
			));
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Contact Page',
				'menu_title' 	=> 'Contact Page',
				'menu_slug'     => 'contact-page',
				'parent_slug' 	=> $parent['menu_slug'],
			));
		}
		public static function change_slideshow_button_label(){
			?>
				<script type="text/javascript">
				(function($) {
					$('.acf-field-ocxvhizcn a.acf-gallery-add').text('Add To Slideshow');
				})(jQuery);	
				</script>
			<?php
		}
		public static function action_acf_update_value( $value, $post_id, $field ){
			$bloginfo_fields = array(
				'field_adf0dijafah123sda',
				'field_adfs9jisdaf12a'
			);
			
			// build bloginfo from acf
			if( in_array($field['key'], $bloginfo_fields) ){
				switch( $field['key'] ) {
					case $bloginfo_fields[0]:
						update_option( 'blogname', $value );
						break;
					case $bloginfo_fields[1]:
						update_option( 'blogdescription', $value );
						break;
				}
			}

			return $value;
		}
	}
	ACFHandler::_init();


















?>