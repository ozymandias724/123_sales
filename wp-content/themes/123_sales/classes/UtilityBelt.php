<?php 
	
	class UtilityBelt{
		public static function load_sections($sections){
			foreach( $sections as $index => $section ) {
				Include( locate_template( 'sections/' . $section . '.php' ) );
			}
		}
		public static function get_gallery_sites(){
			if( UtilityBelt::is_123() ){
				UtilityBelt::get_all_gallery_site_meta();
			}
			elseif( UtilityBelt::is_localhost() ){
				UtilityBelt::get_all_gallery_site_meta();
			}
			else{
				return json_decode(file_get_contents('http://123websites.com/wp-content/themes/123_sales/sites.json'));
			}
		}
		public static function is_123(){
			return strpos(get_option('home'), '123websites.com') !== false ? true : false;
		}
		public static function is_localhost(){
			return strpos(get_option('home'), 'localhost') !== false ? true : false;
		}
		public static function get_all_gallery_site_meta($offset = 0, $posts_per_page = -1, $cat = null){
			$args = array(
				'post_type' => 'site_gallery',
				'posts_per_page' => $posts_per_page,
				'post_status' => 'publish',
				'offset' => $offset,
			);
			if( !is_null($cat) ){
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'gallery_category',
						'field' => 'term_id',
						'terms' => $cat,
					),
				);
			}
			$posts = get_posts($args);
			$arr = [];
			foreach($posts as $post){
				$temparr = [];
				$temparr['id'] = $post->ID;
				$temparr['title'] = $post->post_title;
				$temparr['image'] = get_field('image', $post->ID);
				$temparr['url'] = get_field('url', $post->ID);
				$temparr['description'] = get_field('description', $post->ID);
				$temparr['categories'] = wp_get_post_terms( $post->ID, 'gallery_category' );
				$temparr['on-home-page'] = get_field('on-home-page', $post->ID);
				array_push($arr, $temparr);
			}
			return $arr;
		}
	}

?>