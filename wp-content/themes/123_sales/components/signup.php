<div class="signupsection">
	<div class="signupsection-wrapper">
		<i class="signupsection-wrapper-close fa fa-times"></i>
		<h2 class="signupsection-wrapper-header">Sign Up</h2>
		<?php echo do_shortcode( '[gravityform id="1" ajax="true" title="false" description="false"]' ); ?>
	</div>
</div>