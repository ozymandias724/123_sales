<nav class="nav">
	<div class="nav-wrapper">
		<div class="nav-wrapper-logowrapper">
			<a class="nav-wrapper-logowrapper-link" href="<?php echo site_url(); ?>">
				<img class="nav-wrapper-logowrapper-link-logo" src="<?php the_field('logo', 'option') ?>">
			</a>
		</div>
		<?php 

			require get_template_directory() . '/classes/NavWalker.php';

			wp_nav_menu(array(
				'container' => null,
				'container_class' => null,
				'items_wrap' => '<ul class="nav-wrapper-menu">%3$s</ul>',
				'walker' => new NavWalker(),
			));

		?>
	</div>

</nav>
<div class="mobilenav">
	<div class="mobilenav-wrapper">
		<div class="mobilenav-wrapper-left">
			<a class="mobilenav-wrapper-left-link" href="<?php echo site_url(); ?>">
				<img class="mobilenav-wrapper-left-link-logo" src="<?php the_field('logo', 'option'); ?>">
			</a>
		</div>
		<div class="mobilenav-wrapper-right">
			<i class="mobilenav-wrapper-right-navtoggle fa fa-bars"></i>
		</div>
	</div>
	<div class="mobilenav-tint"></div>
</div>