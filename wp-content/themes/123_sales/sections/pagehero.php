<?php 
	global $template;
	$page_name = str_replace( '.php', '', str_replace( 'page-', '', basename($template) ) );
?>
<section class="pagehero">
	<div class="pagehero-textcontainer">
		<h2 class="pagehero-textcontainer-tagline"><?php the_field($page_name . '-hero-tagline', 'option') ?></h2>
		<?php if( !get_field($page_name . '-disable-hero-button', 'option') ): ?>
			<a<?php echo !empty(get_field($page_name . '-hero-button-link', 'option')['target']) ? ' target="' . get_field($page_name . '-hero-button-link', 'option')['target'] . '"' : ''; ?> href="<?php echo get_field($page_name . '-hero-button-link', 'option')['url'] ?>" class="pagehero-textcontainer-button"><?php echo get_field($page_name . '-hero-button-link', 'option')['title']; ?></a>
		<?php endif; ?>
	</div>
	<div class="pagehero-tint"></div>
	<div class="pagehero-imagecontainer">
		<div<?php echo !empty(get_field($page_name . '-hero-image', 'option')) ? ' style="background-image: url(' . get_field($page_name . '-hero-image', 'option') . ');"' : '';  ?> class="pagehero-imagecontainer-image"></div>
	</div>
</section>