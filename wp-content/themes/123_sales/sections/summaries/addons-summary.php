<?php 
	if( have_rows('addons', 'option') ):
?>
<section class="section addonssummary<?php echo $index % 2 == 0 ? ' grey' : ''; ?>">
	<div class="section-wrapper addonssummary-wrapper">
		<h3 class="section-wrapper-superheader"><?php the_field('addons_superheader', 'option'); ?></h3>
		<h1 class="section-wrapper-header addonssummary-wraapper-header"><?php the_field('addons_header', 'option'); ?></h1>
		<div class="addonssummary-wrapper-grid">
		<?php $count = 0; ?>
		<?php while( have_rows('addons', 'option') ): the_row(); ?>
			<?php if($count == 6){
				continue;
			} ?>
			<div class="addonssummary-wrapper-grid-item">
				<div class="addonssummary-wrapper-grid-item-left">
					<img src="<?php the_sub_field('image'); ?>" class="addonssummary-wrapper-grid-item-left-image">
				</div>
				<div class="addonssummary-wrapper-grid-item-right">
					<h2 class="addonssummary-wrapper-grid-item-right-header"><?php the_sub_field('title'); ?></h2>
					<div class="addonssummary-wrapper-grid-item-right-description">
						<?php the_sub_field('description') ?>
					</div>
					<div class="addonssummary-wrapper-grid-item-right-price">
						<?php echo '<strong>$' . get_sub_field('price') . '</strong> per Site'; ?>
					</div>
				</div>
			</div>
			<?php $count++; ?>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>